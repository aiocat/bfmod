# bfmod

Brainfuck lexer and compiler library for Rust.

## Links

- [Online Documentation](https://docs.rs/bfmod/)
- [Crates.io](https://crates.io/crates/bfmod)

## Example Usage

```rust
use bfmod;

fn main() {
    println!("Starting to lexerize...");
    let tokens = bfmod::lexer::execute(">++++++++[<+++++++++>-]<.>++++[<+++++++>-]<+.+++++++..+++.>>++++++[<+++++++>-]<++.------------.>++++++[<+++++++++>-]<+.<.+++.------.--------.>>>++++[<++++++++>-]<+.");
    println!("Lexerizing is finished.");
    println!("Checking syntax errors.");

    match bfmod::lexer::check_brackets(&tokens) {
        Ok(()) => {
            println!("Building the source code.");
            bfmod::compiler::build(&tokens, "compiled.rs");
        },
        Err(msg) => {
            println!("Error ({})", msg)
        }
    }
}
```

## Found a Bug / Error?

If you found a bug or an error, please create a new issue at gitlab repository.

## Contributing

If you want to contribute this project:

- Make sure you add the comments for your codes.
- Please do not something useless.

## Authors

- [Aiocat](https://gitlab.com/aiocat)

## License

This project is distributed under MIT license.

## Project status

Under development.
